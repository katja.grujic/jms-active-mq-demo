ActiveMQ-Simple-App

Start JmsTest for demonstration.

###Tests:
 - 1, 2 - Queue - Send and receive, using low/high priority as a property 
 - 3 - Topic - Send and receive
 - 4 - Queue - Reply to sender
 - 5 - Queue - Listener receives a message and sends another message to the next listener (message chaining)