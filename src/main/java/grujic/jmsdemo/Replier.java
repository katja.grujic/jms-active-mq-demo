package grujic.jmsdemo;

import grujic.jmsdemo.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class Replier {

    private static final Logger LOGGER = LoggerFactory.getLogger(Replier.class);

    // For testing purposes
    private CountDownLatch latch = new CountDownLatch(5);

    public CountDownLatch getLatch() {
        return latch;
    }

    @JmsListener(destination = "${queue.product.truck}",
            containerFactory = "jmsListenerReplierContainerFactory")
    @SendTo("${queue.product.truck-reply}")
    public String receiveFromQueueAndReplyToListener(Product product) {
        LOGGER.info("Received product='{}' and replying.", product);
        latch.countDown();
        String reply = "Truck received!";
        return reply;
    }

    @JmsListener(destination = "${queue.product.truck-reply}",
            containerFactory = "jmsListenerContainerFactory")
    public void receiveReply(String reply) {
        LOGGER.info("Reply: '{}'", reply);
        latch.countDown();
    }
}
