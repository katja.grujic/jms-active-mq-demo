package grujic.jmsdemo.config;

import grujic.jmsdemo.msgConverter.ProductMessageConverter;
import grujic.jmsdemo.msgConverter.ProductReplyMessageConverter;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;

@Configuration
@EnableJms
public class ReceiverConfig {

    @Value("${spring.activemq.broker-url}")
    private String brokerUrl;

    @Bean
    public ActiveMQConnectionFactory receiverActiveMQConnectionFactory() {
        ActiveMQConnectionFactory activeMQConnectionFactory =
                new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(brokerUrl);

        return activeMQConnectionFactory;
    }

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory(DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, receiverActiveMQConnectionFactory());

        return factory;
    }

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerQueueContainerFactory(final ProductMessageConverter productMessageConverter, DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, receiverActiveMQConnectionFactory());
        factory.setMessageConverter(productMessageConverter);

        return factory;
    }

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerReplierContainerFactory(final ProductReplyMessageConverter productReplyMessageConverter, DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, receiverActiveMQConnectionFactory());
        factory.setMessageConverter(productReplyMessageConverter);

        return factory;
    }

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerTopicContainerFactory(final ProductMessageConverter productMessageConverter, DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, receiverActiveMQConnectionFactory());
        factory.setMessageConverter(productMessageConverter);
        factory.setPubSubDomain(true);

        return factory;
    }

}
