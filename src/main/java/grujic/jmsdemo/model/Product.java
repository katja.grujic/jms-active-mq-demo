package grujic.jmsdemo.model;

import java.time.LocalDateTime;

public class Product {

    private String name;
    private double price;
    private LocalDateTime timestamp;

    public Product() {
    }

    public Product(String name, double price, LocalDateTime timestamp) {
        this.name = name;
        this.price = price;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", timestamp=" + timestamp +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
