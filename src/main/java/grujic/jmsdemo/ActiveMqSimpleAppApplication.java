package grujic.jmsdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActiveMqSimpleAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActiveMqSimpleAppApplication.class, args);
	}

}
