package grujic.jmsdemo;

import grujic.jmsdemo.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

@Component
public class Sender {

    private static final Logger LOGGER = LoggerFactory.getLogger(Sender.class);

    private JmsTemplate jmsTemplate;
    private JmsTemplate jmsTemplateQueue;
    private JmsTemplate jmsTemplateTopic;

    @Autowired
    public Sender(JmsTemplate jmsTemplate, JmsTemplate jmsTemplateQueue, JmsTemplate jmsTemplateTopic) {
        this.jmsTemplate = jmsTemplate;
        this.jmsTemplateQueue = jmsTemplateQueue;
        this.jmsTemplateTopic = jmsTemplateTopic;
    }

    public void sendToQueue(String destination, Product product, boolean isHighPriority) {
        LOGGER.info("Sending product='{}' to queue destination='{}' with highPriority='{}'.", product,
                destination, isHighPriority);

        if (isHighPriority) {
            jmsTemplateQueue.convertAndSend(destination, product,
                    messagePostProcessor -> {
                        messagePostProcessor.setStringProperty("priority",
                                "high");
                        return messagePostProcessor;
                    });
        } else {
            jmsTemplateQueue.convertAndSend(destination, product,
                    messagePostProcessor -> {
                        messagePostProcessor.setStringProperty("priority",
                                "low");
                        return messagePostProcessor;
                    });
        }
    }

    public void sendToTopic(String destination, Product product) {
        LOGGER.info("Sending product='{}' to topic destination='{}'.", product, destination);
        jmsTemplateTopic.convertAndSend(destination, product);
    }

    public void sendToQueueAndReceiveReplyHere(String destination, Product product) throws JMSException {
        LOGGER.info("Sending product='{}' to queue destination='{}' and awaiting reply.", product, destination);
        Message repliedMsg = jmsTemplateQueue.sendAndReceive(destination,
                session -> jmsTemplateQueue.getMessageConverter().toMessage(product, session));
        LOGGER.info(((TextMessage) repliedMsg).getText());
//        Product repliedProduct = (Product) jmsTemplateQueue.getMessageConverter().fromMessage(repliedMsg);
//        LOGGER.info("Received reply='{}'.", repliedProduct);
    }

    public void sendToQueueAndReceiveReplyInListener(String destination, Product product) throws JMSException {
        LOGGER.info("Sending product='{}' to queue destination='{}' and awaiting reply.", product, destination);
        jmsTemplateQueue.convertAndSend(destination, product);
    }

}
