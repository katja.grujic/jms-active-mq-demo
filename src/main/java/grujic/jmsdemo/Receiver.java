package grujic.jmsdemo;

import grujic.jmsdemo.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.concurrent.CountDownLatch;

@Component
public class Receiver {

    private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);

    private JmsTemplate jmsTemplateQueue;

    // For testing purposes
    private CountDownLatch latch = new CountDownLatch(5);

    public Receiver(JmsTemplate jmsTemplateQueue) {
        this.jmsTemplateQueue = jmsTemplateQueue;
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    // A message selector cannot select messages on the basis of the content of the message body.
    @JmsListener(destination = "${queue.product.car}",
                selector = "priority = 'high'",
                containerFactory = "jmsListenerQueueContainerFactory")
    public void receiveHighPriorityFromQueue(@Payload Product product,
                            @Headers MessageHeaders headers,
                            Message message) {
        LOGGER.info("received '{}' priority product='{}'", headers.get("priority"), product);
        latch.countDown();
    }

    @JmsListener(destination = "${queue.product.car}",
            selector = "priority = 'low'",
            containerFactory = "jmsListenerQueueContainerFactory")
    public void receiveLowPriorityFromQueue(@Payload Product product,
                                            @Headers MessageHeaders headers,
                                            Message message) {
        LOGGER.info("received '{}' priority product='{}'", headers.get("priority"), product);
        latch.countDown();
    }

    @JmsListener(destination = "${topic.product.airplane}",
            containerFactory = "jmsListenerTopicContainerFactory")
    public void receiveFromTopic1(@Payload Product product,
                                  @Headers MessageHeaders headers,
                                  Message message) {
        LOGGER.info("Topic subscriber 1 received product='{}'", product);
        latch.countDown();
    }

    @JmsListener(destination = "${topic.product.airplane}",
            containerFactory = "jmsListenerTopicContainerFactory")
    public void receiveFromTopic2(@Payload Product product,
                                  @Headers MessageHeaders headers,
                                  Message message) {
        LOGGER.info("Topic subscriber 2 received product='{}'", product);
        latch.countDown();
    }

    @JmsListener(destination = "${queue.product.bike}",
            containerFactory = "jmsListenerQueueContainerFactory")
    public void receiveFromQueueAndReplyToSender(@Payload Product product,
                                         @Headers MessageHeaders headers,
                                         Message message) throws JMSException {
        LOGGER.info("Received product='{}' and replying.", product);
        String reply = "Bike received!";
        jmsTemplateQueue.send(message.getJMSReplyTo(), session -> session.createTextMessage(reply));
//        Product replyProduct = new Product("Bike REPLY", 3.0, LocalDateTime.now());
//        jmsTemplateQueue.convertAndSend(message.getJMSReplyTo(), replyProduct);
        latch.countDown();
    }

}
