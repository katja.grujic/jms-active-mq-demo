package grujic.jmsdemo.msgConverter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import grujic.jmsdemo.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Component;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

@Component
public class ProductMessageConverter implements MessageConverter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductMessageConverter.class);

    ObjectMapper mapper;

    @Autowired
    public ProductMessageConverter(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public Message toMessage(Object object, Session session) throws JMSException, MessageConversionException {
        Product product = (Product) object;
        String payload = null;
        LOGGER.info("product='{}'", product);
        try {
            payload = mapper.writeValueAsString(product);
            LOGGER.info("outbound json='{}'", payload);
        } catch (JsonProcessingException e) {
            LOGGER.error("error converting from product", e);
        }

        TextMessage message = session.createTextMessage();
        message.setText(payload);

        return message;
    }

    @Override
    public Object fromMessage(Message message) throws JMSException, MessageConversionException {
        TextMessage textMessage = (TextMessage) message;
        String payload = textMessage.getText();
        LOGGER.info("inbound json='{}'", payload);

        Product product = null;
        try {
            product = mapper.readValue(payload, Product.class);
            LOGGER.info("product='{}'", product);
        } catch (Exception e) {
            LOGGER.error("error converting to product", e);
        }

        return product;
    }
}
