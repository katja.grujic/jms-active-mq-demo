package grujic.jmsdemo;

import grujic.jmsdemo.model.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JmsTest {

    @Autowired
    private Sender sender;

    @Autowired
    private Receiver receiver;

    @Autowired
    private Replier replier;

    @Test
    public void testReceiveHighPriorityFromQueue() throws Exception {
        long count = receiver.getLatch().getCount();
        Product product1 = new Product("Car 1", 15.0, LocalDateTime.now());
        sender.sendToQueue("car.q", product1, true);

        receiver.getLatch().await(3L, TimeUnit.SECONDS);
        assertThat(receiver.getLatch().getCount()).isEqualTo(count - 1);
    }

    @Test
    public void testReceiveLowPriorityFromQueue() throws Exception {
        long count = receiver.getLatch().getCount();
        Product product2 = new Product("Car 2", 10.0, LocalDateTime.now());
        sender.sendToQueue("car.q", product2, false);

        receiver.getLatch().await(3L, TimeUnit.SECONDS);
        assertThat(receiver.getLatch().getCount()).isEqualTo(count - 1);
    }

    @Test
    public void testReceiveFromTopic() throws Exception {
        long count = receiver.getLatch().getCount();
        Product product3 = new Product("Airplane", 2345.0, LocalDateTime.now());
        sender.sendToTopic("airplane.t", product3);

        receiver.getLatch().await(3L, TimeUnit.SECONDS);
        assertThat(receiver.getLatch().getCount()).isEqualTo(count - 2);
    }

    @Test
    public void testReceiveFromQueueAndReplyToSender() throws Exception {
        long count = receiver.getLatch().getCount();
        Product product4 = new Product("Bike", 3.0, LocalDateTime.now());
        sender.sendToQueueAndReceiveReplyHere("bike.q", product4);

        receiver.getLatch().await(3L, TimeUnit.SECONDS);
        assertThat(receiver.getLatch().getCount()).isEqualTo(count - 1);
    }

    @Test
    public void testReceiveFromQueueAndReplyToListener() throws Exception {
        long count = receiver.getLatch().getCount();
        Product product5 = new Product("Truck", 399.0, LocalDateTime.now());
        sender.sendToQueueAndReceiveReplyInListener("truck.q", product5);

        receiver.getLatch().await(3L, TimeUnit.SECONDS);
        assertThat(replier.getLatch().getCount()).isEqualTo(count - 2);
    }

}
